FROM node:16.13-alpine AS build 
WORKDIR /dist/src/app

RUN npm cache clean --force

COPY . . 

RUN npm install -g @angular/cli
RUN npm install
RUN npm run build --prod

FROM nginx:latest AS prod
COPY --from=build /dist/src/app/dist/mds-dds-exam-angular /usr/share/nginx/html

EXPOSE 80