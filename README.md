## Init your env

Run `npm install -g @angular/cli`
Run `npm install`

## Development server

Run `ng serve --port 8081` for a dev server. Navigate to `http://localhost:8081/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Docker

- Ouvrir un terminal à la racine du repo local front
- lancer pour créer l'image (le nom présenté est important car elle est utilisé dans le compose)

```
docker build -t deploy-solution-front .
```

- puis

```
docker run -d -p 80:80 --name front-angular deploy-solution-front
```

Le container est à présent lancé et accessible via "localhost:80".
